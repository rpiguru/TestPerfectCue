import glob
import ntpath
import os
import platform


_cur_dir = os.path.dirname(os.path.realpath(__file__))


if __name__ == '__main__':

    for ui_file in glob.glob(os.path.join(_cur_dir, '*.ui')):
        name = ntpath.basename(ui_file).split('.')[0]
        py_file = os.path.join(_cur_dir, f'ui_{name}.py')
        os.system(f"pyside2-uic {ui_file} > {py_file}")
        # A bug in PySide2?
        if platform.system() == 'Linux':
            os.system(f"sed -i -- \"s/QString()/''/g\" {py_file}")

    # os.system(f"pyside2-rcc -o perfectcue_rc.py perfectcue.qrc")
