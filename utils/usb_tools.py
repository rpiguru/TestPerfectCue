import time

import pywinusb.hid as hid

from utils.logger import logger


def find_all_devices():
    devices = []
    for d in hid.HidDeviceFilter().get_devices():
        info = dict(
            vendor_id=d.vendor_id,
            vendor_name=d.vendor_name,
            product_id=d.product_id,
            product_name=d.product_name,
            str=f"{d.product_name}(0x{d.product_id:04x}) - {d.vendor_name}(0x{d.vendor_id:04x})",
        )
        if info not in devices:
            devices.append(info)
    return devices


class HIDDevice(object):

    def __init__(self, vendor_id, product_id, callback=None):
        _filter = hid.HidDeviceFilter(vendor_id=vendor_id, product_id=product_id)
        hid_device = _filter.get_devices()
        self.dev = hid_device[0]
        self.dev.open()
        self.dev.set_raw_data_handler(self._on_raw_data_received)
        self.callback = callback

    def get_all_reports(self):
        return self.dev.find_any_reports()

    def send_report(self, data: bytearray):
        report = self.dev.find_output_reports()[0]
        # Total length is 65
        buf = [0] * 65
        buf[0] = report.report_id
        for i, d in enumerate(data):
            buf[1 + i] = d
        report.set_raw_data(buf)
        try:
            return report.send()
        except Exception as e:
            logger.exception(f"Failed to send USB data - {e}")

    def _on_raw_data_received(self, data):
        if any(data) and callable(self.callback):
            self.callback(data)

    def close(self):
        self.dev.close()


if __name__ == '__main__':

    def on_raw_data_read(data):
        print(f"RAW data: {data}")

    _dev = HIDDevice(vendor_id=0x483, product_id=0x101A, callback=on_raw_data_read)

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break

    _dev.close()
