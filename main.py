import datetime
import sys
from functools import partial

import qdarkstyle
from PySide2.QtCore import Signal, QTimer
from PySide2.QtWidgets import QMainWindow, QApplication, QStyledItemDelegate

from ui_perfect_cue import Ui_TestPerfectCue
from utils.logger import logger
from utils.usb_tools import find_all_devices, HIDDevice


class MainWindow(QMainWindow):

    received = Signal(str)

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_TestPerfectCue()
        self.ui.setupUi(self)
        self.ui.txtCommand.setInputMask("Hh hh hh hh hh hh hh hh")
        self.ui.btnRefresh.released.connect(self._on_btn_refresh)
        self.ui.btnSend.released.connect(self._on_btn_send)
        self.ui.btnStop.setDisabled(True)
        self.ui.btnStop.released.connect(self._on_btn_stop)
        self.ui.comboDevices.setItemDelegate(QStyledItemDelegate())
        self.ui.comboDevices.currentIndexChanged.connect(self._on_device_selected)
        self.devices = []
        self._device = None
        self.timer = None
        self.received.connect(self._on_received)

    def _on_btn_refresh(self):
        self.devices = find_all_devices()
        self.ui.comboDevices.clear()
        self.ui.comboDevices.addItems([''] + [d['str'] for d in self.devices])

    def _on_btn_send(self):
        cmd = self.ui.txtCommand.text().replace(' ', '')
        byte_data = bytes.fromhex(cmd)
        self.timer = QTimer()
        self.timer.timeout.connect(partial(self._send_data, byte_data))
        self.timer.start(100)
        self.ui.btnSend.setDisabled(True)
        self.ui.btnStop.setDisabled(False)
        self.ui.txtCommand.setDisabled(True)
        self.statusBar().showMessage("Sending command...")

    def _send_data(self, byte_data):
        if self._device is not None:
            result = self._device.send_report(byte_data)
            logger.debug(f"Sent command({byte_data}) - {result}")

    def _on_btn_stop(self):
        if self.timer is not None:
            self.timer.stop()
            self.timer = None
        self.ui.btnStop.setDisabled(True)
        self.ui.btnSend.setDisabled(False)
        self.ui.txtCommand.setDisabled(False)
        self.statusBar().showMessage("Stop")

    def _on_device_selected(self):
        index = self.ui.comboDevices.currentIndex()
        if index > 0:
            dev_info = self.devices[index - 1]
            logger.debug(f"{dev_info['str']} is selected")
            if self._device is not None and hasattr(self._device, 'close'):
                self._device.close()
            self._device = HIDDevice(vendor_id=dev_info['vendor_id'], product_id=dev_info['product_id'],
                                     callback=self._on_data_from_hid_device)

    def _on_data_from_hid_device(self, data):
        data = [f"{i:02X}" for i in data]
        logger.info(f"Data received - {data}")
        self.received.emit(' '.join(data))

    def _on_received(self, data):
        self.ui.txtResult.appendPlainText(f'{datetime.datetime.now()} | {data}')


if __name__ == '__main__':

    logger.info('========== Starting PerfectCue Tester Application ==========')

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, traceback):
        logger.error(f'========== Crashed! Exctype: {exctype}, Value: {value}, Traceback: {traceback}')
        getattr(sys, "_excepthook")(exctype, value, traceback)
        sys.exit(1)

    sys.excepthook = exception_hook

    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
