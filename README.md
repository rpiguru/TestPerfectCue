# TestPerfectCue

Windows Testing Application for PerfectCue device thru VideoClock-2000PC USB-RS485 dongle

## Installation

1. Download **Python3.7** and install.

2. Clone this repository
```shell script
git clone https://gitlab.com/rpiguru/TestPerfectCue
```

3. Install dependencies
```shell script
cd testPerfectCue
python -m pip install -r requirements.txt
```


## Package into a single executable file.

```shell script
python -m PyInstaller --onefile main.spec
```
