# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'perfect_cue.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_TestPerfectCue(object):
    def setupUi(self, TestPerfectCue):
        if not TestPerfectCue.objectName():
            TestPerfectCue.setObjectName(u"TestPerfectCue")
        TestPerfectCue.resize(496, 308)
        self.action_New = QAction(TestPerfectCue)
        self.action_New.setObjectName(u"action_New")
        self.actionE_xit = QAction(TestPerfectCue)
        self.actionE_xit.setObjectName(u"actionE_xit")
        self.actionRefresh = QAction(TestPerfectCue)
        self.actionRefresh.setObjectName(u"actionRefresh")
        self.actionAbout = QAction(TestPerfectCue)
        self.actionAbout.setObjectName(u"actionAbout")
        self.centralwidget = QWidget(TestPerfectCue)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.horizontalLayout_2 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(5, 5, 5, 5)
        self.comboDevices = QComboBox(self.groupBox_2)
        self.comboDevices.setObjectName(u"comboDevices")
        self.comboDevices.setMinimumSize(QSize(0, 30))

        self.horizontalLayout_2.addWidget(self.comboDevices)

        self.btnRefresh = QToolButton(self.groupBox_2)
        self.btnRefresh.setObjectName(u"btnRefresh")
        self.btnRefresh.setMinimumSize(QSize(0, 30))

        self.horizontalLayout_2.addWidget(self.btnRefresh)


        self.verticalLayout_2.addWidget(self.groupBox_2)

        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.txtCommand = QLineEdit(self.groupBox)
        self.txtCommand.setObjectName(u"txtCommand")
        self.txtCommand.setMinimumSize(QSize(0, 30))

        self.horizontalLayout.addWidget(self.txtCommand)

        self.btnSend = QToolButton(self.groupBox)
        self.btnSend.setObjectName(u"btnSend")
        self.btnSend.setMinimumSize(QSize(0, 30))

        self.horizontalLayout.addWidget(self.btnSend)

        self.btnStop = QToolButton(self.groupBox)
        self.btnStop.setObjectName(u"btnStop")
        self.btnStop.setMinimumSize(QSize(0, 30))

        self.horizontalLayout.addWidget(self.btnStop)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.txtResult = QPlainTextEdit(self.groupBox)
        self.txtResult.setObjectName(u"txtResult")
        self.txtResult.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.verticalLayout.addWidget(self.txtResult)


        self.verticalLayout_2.addWidget(self.groupBox)

        TestPerfectCue.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(TestPerfectCue)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 496, 23))
        self.menu_File = QMenu(self.menubar)
        self.menu_File.setObjectName(u"menu_File")
        self.menu_Help = QMenu(self.menubar)
        self.menu_Help.setObjectName(u"menu_Help")
        self.menuAction = QMenu(self.menubar)
        self.menuAction.setObjectName(u"menuAction")
        TestPerfectCue.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(TestPerfectCue)
        self.statusbar.setObjectName(u"statusbar")
        TestPerfectCue.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menu_File.menuAction())
        self.menubar.addAction(self.menuAction.menuAction())
        self.menubar.addAction(self.menu_Help.menuAction())
        self.menu_File.addAction(self.action_New)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.actionE_xit)
        self.menu_Help.addAction(self.actionAbout)
        self.menuAction.addAction(self.actionRefresh)

        self.retranslateUi(TestPerfectCue)

        QMetaObject.connectSlotsByName(TestPerfectCue)
    # setupUi

    def retranslateUi(self, TestPerfectCue):
        TestPerfectCue.setWindowTitle(QCoreApplication.translate("TestPerfectCue", u"PerfectCue Testing App", None))
        self.action_New.setText(QCoreApplication.translate("TestPerfectCue", u"&New", None))
        self.actionE_xit.setText(QCoreApplication.translate("TestPerfectCue", u"E&xit", None))
#if QT_CONFIG(shortcut)
        self.actionE_xit.setShortcut(QCoreApplication.translate("TestPerfectCue", u"Ctrl+X", None))
#endif // QT_CONFIG(shortcut)
        self.actionRefresh.setText(QCoreApplication.translate("TestPerfectCue", u"Refresh", None))
        self.actionAbout.setText(QCoreApplication.translate("TestPerfectCue", u"About", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("TestPerfectCue", u"Select Device", None))
        self.btnRefresh.setText(QCoreApplication.translate("TestPerfectCue", u"Refresh", None))
        self.groupBox.setTitle(QCoreApplication.translate("TestPerfectCue", u"Test", None))
        self.btnSend.setText(QCoreApplication.translate("TestPerfectCue", u"Send", None))
        self.btnStop.setText(QCoreApplication.translate("TestPerfectCue", u"Stop", None))
        self.txtResult.setPlainText("")
        self.menu_File.setTitle(QCoreApplication.translate("TestPerfectCue", u"&File", None))
        self.menu_Help.setTitle(QCoreApplication.translate("TestPerfectCue", u"&Help", None))
        self.menuAction.setTitle(QCoreApplication.translate("TestPerfectCue", u"Action", None))
    # retranslateUi

