import os

BASE_DIR = os.path.expanduser('~/.pc')
os.makedirs(BASE_DIR, exist_ok=True)


VC_PID = 0x101A
VC_VID = 0x483

try:
    from local_settings import *
except ImportError:
    pass
